How to use
==========
1. Copy module to sites/<site>/modules/.
2. Enable module admin/build/modules.
3. You can use chroma hash by add '#chromahash' = TRUE to element.
  For example:
  $form['password'] = array(
    '#type' => 'password',
    '#title' => 'password',
    '#chromahash' => TRUE,
  );
